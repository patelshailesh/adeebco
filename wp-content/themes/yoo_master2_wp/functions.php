<?php
/**
* @package   Master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// check compatibility
if (version_compare(PHP_VERSION, '5.3', '>=')) {

    // bootstrap warp
    require(__DIR__.'/warp.php');
}

/* function wpcf7_dynamic_email_field($components) {

  if((isset($components['sender']) && $components['sender'] != 'no-reply@ramworld.net') && isset($_POST) && (isset($_POST['contact-topic']) && ($_POST['contact-topic'] =='Sales' || $_POST['contact-topic'] =='مبيعات'))){
     $components['recipient'] = 'sales@adeeb.com';
  } elseif(isset($components['sender']) && $components['sender'] != 'no-reply@ramworld.net') && isset($_POST) && (isset($_POST['contact-topic']) && ($_POST['contact-topic'] =='Purchases' || $_POST['contact-topic'] =='مشتريات'))){
     $components['recipient'] = 'supplier@adeeb.com';
  } elseif(isset($components['sender']) && $components['sender'] != 'no-reply@ramworld.net') && isset($_POST) && (isset($_POST['contact-topic']) && ($_POST['contact-topic'] =='Complaints'|| $_POST['contact-topic'] =='شكاوى'))){
     $components['recipient'] = 'cc@adeeb.com';
  } elseif(isset($components['sender']) && $components['sender'] != 'no-reply@ramworld.net') && isset($_POST) && (isset($_POST['contact-topic']) && ($_POST['contact-topic'] =='Others'|| $_POST['contact-topic'] =='أخرى'))){
  //   $components['recipient'] = 'info@adeeb.com';
     $components['recipient'] = "sagar@avdevs.com";
  }

return $components;
} */

function wpcf7_dynamic_email_field($components) {

  if((isset($components['sender']) && $components['sender'] != 'no-reply@ramworld.net') && isset($_POST) && (isset($_POST['contact-topic']) && ($_POST['contact-topic'] =='Sales' || $_POST['contact-topic'] =='مبيعات'))){
     $components['recipient'] = 'sales@adeeb.com';
  } elseif((isset($components['sender']) && $components['sender'] != 'no-reply@ramworld.net') && isset($_POST) && (isset($_POST['contact-topic']) && ($_POST['contact-topic'] =='Purchases' || $_POST['contact-topic'] =='مشتريات'))){
     $components['recipient'] = 'supplier@adeeb.com';
  } elseif((isset($components['sender']) && $components['sender'] != 'no-reply@ramworld.net') && isset($_POST) && (isset($_POST['contact-topic']) && ($_POST['contact-topic'] =='Complaints'|| $_POST['contact-topic'] =='شكاوى'))){
     $components['recipient'] = 'cc@adeeb.com';
  } elseif((isset($components['sender']) && $components['sender'] != 'no-reply@ramworld.net') && isset($_POST) && (isset($_POST['contact-topic']) && ($_POST['contact-topic'] =='Others'|| $_POST['contact-topic'] =='أخرى'))){
     $components['recipient'] = 'info@adeeb.com';
  } 
	return $components;
}

add_filter('wpcf7_mail_components', 'wpcf7_dynamic_email_field'); 
function custom_gallery(){
    if ( is_user_logged_in() ) {
        echo "<style>.custom-products{ display: block;}</style>";
    } else {
        echo "<style>.custom-products{ display: none;}</style>";
    }
}
add_action("wp_head", 'custom_gallery');
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}